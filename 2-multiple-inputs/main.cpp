#include <iostream>
using namespace std;

void inputNumbers(int n) {
    int x;

    for (int i = 0; i < n; i++) {
        cin >> x;

        cout << "You inputted: " << x << endl;
    }
}

int main() {

    /*
        Ask user to input 3 integers.
        Display each input.

        Example:
        You inputted: 3
        You inputted: 5
        You inputted: 7
    */

    inputNumbers(5);

    return 0;
}