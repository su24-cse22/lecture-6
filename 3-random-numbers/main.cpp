#include <iostream>
#include <ucm_random>
using namespace std;

void generateRandomNumbers(int n) {
    RNG generator;

    for (int i = 0; i < n; i++) {
        int x = generator.get(100, 1000);

        cout << x << endl;
    }
}

int main() {

    /*
        Generate n random numbers between 100 and 1000.

        Example:
        237
        649
        334
        719
        925
    */

    generateRandomNumbers(5);

    return 0;
}