#include <iostream>
using namespace std;

int main() {

    /*
        Ask user for 5 integers.
        Determine how many of them were even and how many were odd.
    */

    // 5 % 2
    // 5 / 2 = 2 rem 1

    // 10 % 2
    // 10 / 2 = 5 rem 0

    int x;
    int evenCount = 0;
    int oddCount = 0;

    for (int i = 0; i < 5; i++) {
        cin >> x;

        if (x % 2 == 0) {
            evenCount++;
        } else {
            oddCount++;
        }
    }

    cout << "The user inputted " << evenCount << " even numbers." << endl;
    cout << "The user inputted " << oddCount << " odd numbers." << endl;

    return 0;
}