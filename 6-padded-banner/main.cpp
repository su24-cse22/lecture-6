#include <iostream>
using namespace std;

void printPaddedBanner(string message) {
    int length = message.length();
    int padding = 6;

    for (int i = 0; i < length + padding; i++) {
        cout << "=";
    }
    cout << endl;

    cout << "   " << message << endl;

    for (int i = 0; i < length + padding; i++) {
        cout << "=";
    }
    cout << endl;
}

int main() {

    /*
        Ask user for message and display a padded banner.
        The padding should be 1 character.

        Example:

        ===============
           UC Merced 
        ===============
    */

    printPaddedBanner("University of California, Merced");

    return 0;
}