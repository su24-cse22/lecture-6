#include <iostream>
using namespace std;

int main() {

    /*
        Ask user to input a number.
        Find the factorial of that number.

        Example:
        
        4! = 4 * 3 * 2 * 1 = 24
    */

    int x;
    cin >> x;

    int factorial = 1;

    cout << x << "! = ";
    for (int i = x; i >= 1; i--) {
        cout << i;


        // factorial = factorial * i;
        factorial *= i;

        if (i > 1) {
            cout << " * ";
        }
    }
    cout << " = " << factorial << endl;

    return 0;
}