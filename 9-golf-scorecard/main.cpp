#include <iostream>
using namespace std;

void printHeader(string courseName) {
    cout << ".---------------------------------------------------------------------------------------------------------------------------------------------------." << endl;
    
    int length = 149;
    int padding = 4;
    cout << "| " << courseName;
    for (int i = 0; i < length - padding - courseName.length(); i++) {
        cout << " ";
    }
    cout << " |" << endl;
    
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
    cout << "| Hole             |  1  |  2  |  3  |  4  |  5  |  6  |  7  |  8  |  9  | OUT  | 10  | 11  | 12  | 13  | 14  | 15  | 16  | 17  | 18  |  IN  | TOT  |" << endl;
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

void printCell(int value) {
    if (value < 100) {
        cout << " " << value << "  |";
    }
    else {
        cout << " " << value << " |";
    }
}

void printYardages(string color, int h1, int h2, int h3, int h4, int h5, int h6, int h7, int h8, int h9, int h10, int h11, int h12, int h13, int h14, int h15, int h16, int h17, int h18) {
    cout << "| " << color;

    int length = 20;
    int padding = 4;
    for (int i = 0; i < length - padding - color.length(); i++) {
        cout << " ";
    }
    
    cout << " |";

    printCell(h1);
    printCell(h2);
    printCell(h3);
    printCell(h4);
    printCell(h5);
    printCell(h6);
    printCell(h7);
    printCell(h8);
    printCell(h9);

    int out = h1 + h2 + h3 + h4 + h5 + h6 + h7 + h8 + h9;
    printCell(out);

    printCell(h10);
    printCell(h11);
    printCell(h12);
    printCell(h13);
    printCell(h14);
    printCell(h15);
    printCell(h16);
    printCell(h17);
    printCell(h18);

    int in = h10 + h11 + h12 + h13 + h14 + h15 + h16 + h17 + h18;
    printCell(in);

    int total = out + in;
    printCell(total);

    cout << endl;
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

void printBlank() {
    cout << "|                  |     |     |     |     |     |     |     |     |     |      |     |     |     |     |     |     |     |     |     |      |      |" << endl;
    cout << "|---------------------------------------------------------------------------------------------------------------------------------------------------|" << endl;
}

int main() {

    /*
        Print generic golf scorecard.
    */

    string courseName;
    getline(cin, courseName);
    printHeader(courseName);

    int blue1, blue2, blue3, blue4, blue5, blue6, blue7, blue8, blue9, blue10, blue11, blue12, blue13, blue14, blue15, blue16, blue17, blue18;
    cin >> blue1 >> blue2 >> blue3 >> blue4 >> blue5 >> blue6 >> blue7 >> blue8 >> blue9 >> blue10 >> blue11 >> blue12 >> blue13 >> blue14 >> blue15 >> blue16 >> blue17 >> blue18;
    printYardages("Blue", blue1, blue2, blue3, blue4, blue5, blue6, blue7, blue8, blue9, blue10, blue11, blue12, blue13, blue14, blue15, blue16, blue17, blue18);

    int white1, white2, white3, white4, white5, white6, white7, white8, white9, white10, white11, white12, white13, white14, white15, white16, white17, white18;
    cin >> white1 >> white2 >> white3 >> white4 >> white5 >> white6 >> white7 >> white8 >> white9 >> white10 >> white11 >> white12 >> white13 >> white14 >> white15 >> white16 >> white17 >> white18;
    printYardages("White", white1, white2, white3, white4, white5, white6, white7, white8, white9, white10, white11, white12, white13, white14, white15, white16, white17, white18);

    int red1, red2, red3, red4, red5, red6, red7, red8, red9, red10, red11, red12, red13, red14, red15, red16, red17, red18;
    cin >> red1 >> red2 >> red3 >> red4 >> red5 >> red6 >> red7 >> red8 >> red9 >> red10 >> red11 >> red12 >> red13 >> red14 >> red15 >> red16 >> red17 >> red18;
    printYardages("Red", red1, red2, red3, red4, red5, red6, red7, red8, red9, red10, red11, red12, red13, red14, red15, red16, red17, red18);

    printBlank();
    printBlank();
    printBlank();
    printBlank();

    return 0;
}