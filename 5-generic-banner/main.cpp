#include <iostream>
using namespace std;

void printBanner(string message) {
    int length = message.length();

    // print top border
    for (int i = 0; i < length; i++) {
        cout << "=";
    }
    cout << endl;

    // print message
    cout << message << endl;

    // print bottom border
    for (int i = 0; i <  length; i++) {
        cout << "=";
    }
    cout << endl;
}

int main() {

    /*
        Ask user for message and display a banner.

        Example:

        =========
        UC Merced
        =========
    */

    printBanner("Computer Science");

    return 0;
}