#include <iostream>
using namespace std;

void printHelloWorld(int n) {
    for (int i = 0; i < n; i++) {
        cout << i << " : Hello World!" << endl;
    }
}

int main() {

    /*
        Print "Hello World!" 5 times.

        Print "Hello World!" n times.
    */

    printHelloWorld(10);

    return 0;
}