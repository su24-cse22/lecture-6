#include <iostream>
using namespace std;

void printFixedWidthBanner(string message) {
    cout << "==================================================" << endl;

    cout << "| " << message;

    int space = 50;
    int padding = 4;
    int length = message.length();

    for (int i = 0; i < space - padding - length; i++) {
        cout << " ";
    }
    cout << " |" << endl;

    cout << "==================================================" << endl;
}

int main() {

    /*
        Ask user for message and display a fixed width banner.
        The width should always be 50 characters.

        Example:

        ==================================================
        | UC Merced                                      |
        ==================================================
    */

    printFixedWidthBanner("University of California, Merced");

    return 0;
}